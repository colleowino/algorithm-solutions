/*
 *  Brute force:
 *  starting from 1 until nth ugly number found,
 *  divide the number by 2,3,5 until you get to 1.
 *  if it isn't possible then it isn't an ugly number.
 *  complexity: N log n but not n^2
 */

/*
 * brute force way to determine if the number can be factorized down to 1
 */
exports.isUgly = (n) => {
  if (n === 0) {
    return false;
  }
  if (n === 1) {
    return true;
  }
  while (n % 2 === 0) {
    n /= 2;
  }
  while (n % 3 === 0) {
    n /= 3;
  }
  while (n % 5 === 0) {
    n /= 5;
  }
  return n === 1;
};

/*
 * Ugly numbers are numbers whose only prime factors are 2, 3 or 5.
 */
exports.uglyNumbers = (n) => {
  /*
   * @approach: bottom up, start with smallest ugly number 1 coz 2^0 * 3^0 * 5^0 = 1
   * @subproblem:
   * - generate the minimum ugly number based on the current powers of 2,3,5
   * - skipping over numbers already calculated
   * @strategy:
   * - start with list of numbers at their highest powers.
   * - pick the minimal ugly number and raise the power power of [2,3,5]
   * ie. if 4 was last smallest or 2^2, multiplying it by 2 makes it 2^3 or 6
   * - pointers [p2,p3,p5] keep track of the last computed multiple
   * When more than one number has the same minumum, then raise both number and update pointers.
   */

  let ugly = null;
  let p2 = 0;
  let p3 = 0;
  let p5 = 0;

  let n2 = 2;
  let n3 = 3;
  let n5 = 5;

  let uglyNums = [1];

  for (let i = 1; i < n; i++) {
    ugly = Math.min(n2, n3, n5);
    uglyNums.push(ugly);

    if (n2 == ugly) {
      p2 += 1;
      n2 = uglyNums[p2] * 2;
    }
    if (n3 == ugly) {
      p3 += 1;
      n3 = uglyNums[p3] * 3;
    }
    if (n5 == ugly) {
      p5 += 1;
      n5 = uglyNums[p5] * 5;
    }
  }

  return ugly;
};
