function print(params) {
  console.log(...arguments);
}

exports.knapsack = (itemCount, weights, costs, totalWeight) => {
  let dp = [];
  let maxValue = 0;

  let initRow = [];
  for (let col = 0; col <= totalWeight; col++) {
    initRow.push(0);
  }
  dp.push(initRow);

  for (let row = 1; row <= itemCount; row++) {
    // init row

    let itemRow = [];
    for (let col = 0; col <= totalWeight; col++) {
      // current item w must fit in column weight

      /*
       * if current weight fits in column limit?
       * Yes:
       * - record its value and calculate remaining weight `q`
       * - use q column of row above to get maximum value obtained from that extra space.
       * No:
       * - use the row above
       */

      let currentWeight = weights[row - 1];
      let cellValue = dp[row - 1][col]; // have old cell value

      if (currentWeight <= col) {
        let currentValue = costs[row - 1];
        let remWeight = col - currentWeight;
        let totalValue = dp[row - 1][remWeight] + currentValue;
        cellValue = Math.max(totalValue, cellValue);
      }

      maxValue = Math.max(maxValue, cellValue);
      itemRow.push(cellValue);
    }
    dp.push(itemRow);
  }
  return [maxValue, dp];
};

exports.knapsackItems = (dp, weights) => {
  /*
   * Values of current cell and cell above differ:
   * YES:
   * - then current included in total. negate the row weight from active_colum.
   * - add row number to list of selected values
   * - use difference to set active_column
   * Default:
   * the row above becomes current_row above
   * repeat until 0 weight
   */
  let row = dp.length;
  let col = dp[0].length;
  let activeCol = col - 1;

  let selectedItems = [];

  for (let activeRow = row - 1; activeRow > 0; activeRow--) {
    let activePrice = dp[activeRow][activeCol];
    if (activePrice == 0) {
      break;
    }
    let cellPriceAbove = dp[activeRow - 1][activeCol];

    if (activePrice != cellPriceAbove) {
      selectedItems.push(activeRow - 1);
      activeCol -= weights[activeRow - 1];
    }
  }

  return selectedItems;
};
