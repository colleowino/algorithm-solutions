/*
 * game plan
 * split units to one item
 * merge back items into the proper place in a temporary array and return it.
 * use indexes of both arrays to pick items
 */

function mergeSort(arr) {
  // base case single item
  let size = arr.length;
  if (size <= 1) {
    return arr;
  }
  // split the array
  let left = mergeSort(arr.slice(0, size / 2));
  let right = mergeSort(arr.slice(size / 2));

  return merge(left, right);
}

function merge(arr1, arr2) {
  let len1 = arr1.length;
  let len2 = arr2.length;

  let lenBoth = len1 + len2;
  let p1 = 0;
  let p2 = 0;

  // init temp array large enough to hold both values
  let tempArr = [];
  for (let i = 0; i < lenBoth; i++) {
    tempArr.push(0);
  }

  for (let j = 0; j < lenBoth; j++) {
    // use the other array if one already exhausted
    if (p1 == len1) {
      tempArr[j] = arr2[p2];
      p2 += 1;
    } else if (p2 == len2) {
      tempArr[j] = arr1[p1];
      p1 += 1;
    } else {
      // base case compare next
      if (arr1[p1] < arr2[p2]) {
        tempArr[j] = arr1[p1];
        p1 += 1;
      } else {
        tempArr[j] = arr2[p2];
        p2 += 1;
      }
    }
  }

  return tempArr;
}

array = [10, 4, 6, 4, 8, -13, 2, 3];

print(mergeSort(array));
// print(merge([1, 5, 8], [3, 6, 1]));
