const { uglyNumbers, isUgly } = require('./uglyNumbers');

describe('generate nth ugly number', () => {
  test('should return 2045 as 1500th ugly number', () => {
    expect(uglyNumbers(1500)).toBe(859963392);
  });

  test('should return 15 as 11 ugly number', () => {
    expect(uglyNumbers(11)).toBe(15);
  });
});

describe('check if number is ugly', () => {
  test('should return 35 is not ugly', () => {
    expect(isUgly(35)).toBe(false);
  });

  test('should return 25 is ugly', () => {
    expect(isUgly(25)).toBe(true);
  });
});
