const { knapsack: sack, knapsackItems } = require('./knapsack');

// case 1
describe('Case 1', () => {
  let items = 5;
  let maxWeight = 7;
  let itemPrices = [2, 2, 4, 5, 3];
  let itemWeights = [3, 1, 3, 4, 2];

  let results = sack(items, itemWeights, itemPrices, maxWeight);

  test('should return 10 as max', () => {
    expect(results[0]).toBe(10);
  });

  test('Case 2 should choose [4, 3, 1]', () => {
    expect(knapsackItems(results[1], itemWeights)).toEqual([4, 3, 1]);
  });
});

describe('Case 2', () => {
  let items = 3;
  let maxWeight = 5;
  let itemPrices = [5, 3, 4];
  let itemWeights = [3, 2, 1];

  let results = sack(items, itemWeights, itemPrices, maxWeight);

  test('should return 9 as max', () => {
    expect(results[0]).toBe(9);
  });

  test('Case 2 should choose [2,0]', () => {
    expect(knapsackItems(results[1], itemWeights)).toEqual([2, 0]);
  });
});
